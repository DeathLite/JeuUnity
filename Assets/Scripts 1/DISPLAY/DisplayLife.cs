﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayLife : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GetComponent<Text>().text = "Life: " + FollowPlayer.nbLife.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Text>().text = "Life :" + FollowPlayer.nbLife.ToString();
    }
}
