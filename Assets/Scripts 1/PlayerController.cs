﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    [HideInInspector]
    public GameObject Player;
    float vitesse = 0.5f;
    // Use this for initialization
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    public RotationAxes axes = RotationAxes.MouseXAndY;
    public float sensitivityX = 15F;
    public float sensitivityY = 15F;
    public float minimumX = -360F;
    public float maximumX = 360F;
    public float minimumY = -60F;
    public float maximumY = 60F;
    float rotationY = 0F;
    public const int INFINITE=1;
    private static PlayerController instance;
	public bool alive;

	void Start () 
	{
		alive = true;
	}

    public static PlayerController Instance
    {
        get
        {
            return instance ?? (instance = new GameObject("PlayerController_objet").AddComponent<PlayerController>());
        }
    }

    public void Awake()
    {
        if (instance != null)
        {
            Debug.Log("Il ne doit pas y avoir deux instances de PlayerControllers (Singleton)");
            Destroy(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    private int p_rideStyle;

    public int GetRideStyle() { return p_rideStyle; }

    public void SetRideStyle(int style) { p_rideStyle = style; }

    // Update is called once per frame
    void Update ()
    {
        if (axes == RotationAxes.MouseXAndY)
        {
            float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(rotationY, rotationX, 0);
        }
        else if (axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
        }
        else
        {
            rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
            rotationY = Mathf.Clamp(-rotationY, minimumY, maximumY);

            transform.localEulerAngles = new Vector3(rotationY, transform.localEulerAngles.y, 0);
        }

        if (Input.GetKey("right") || Input.GetKey("d"))
        {
            right();
        }
        if (Input.GetKey("left")|| Input.GetKey("q"))
        {
            left();
        }
        if (Input.GetKey("space"))
        {

            transform.Translate(0, 0.5f, 0);
        }

        if  (Input.GetKey("up")|| Input.GetKey("z"))
        {
           up();
        }
        if (Input.GetKey("down")|| Input.GetKey("s"))
        {
            down();
        }
    }

    void right()
    {
        transform.Translate(-vitesse, 0, 0);
    }

    void left()
    {
        transform.Translate(vitesse, 0, 0);
    }

    void up()
    {
        transform.Translate(0, 0, -vitesse);
    }

    void down()
    {
        transform.Translate(0, 0, vitesse);
    }
   }
