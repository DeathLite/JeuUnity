﻿using UnityEngine;
using System.Collections;

public class SpawnPlatInvi : MonoBehaviour {

	[SerializeField]
	public GameObject plat;


	// Update is called once per frame
	void Update () {
			if(PickUpEclair.nbEclair==1)
			{
				Spawn ();	
			}
	}

	void Spawn()
	{
		// Create the Bullet 
		GameObject plateforme =  (GameObject) ( Instantiate (plat, transform.position, transform.rotation));
		PickUpEclair.nbEclair = 0;
	}
}
