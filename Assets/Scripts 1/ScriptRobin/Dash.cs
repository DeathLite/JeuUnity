﻿using UnityEngine;
using System.Collections;

public class Dash : MonoBehaviour {

	public float maxDashTime = 6.0f;
	public float dashSpeed = 15.0f;
	public float dashStoppingSpeed = 1.0f;
	public int compteur=0;
	public static bool statement2 = false;

	private float currentDashTime;
	private Vector3 m_MoveDir = Vector3.zero;

	// Use this for initialization
	void Start () 
	{
		currentDashTime = maxDashTime;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (statement2 == true) {
			if (Input.GetKey(KeyCode.F)) {
				currentDashTime = 0.0f;
			}
			if (currentDashTime < maxDashTime) {
				//m_MoveDir = new Vector3 (0.0f, 0.0f, dashSpeed);
				m_MoveDir = transform.forward * dashSpeed;
				currentDashTime += dashStoppingSpeed;
			} else {
				m_MoveDir = Vector3.zero;
			}
			transform.position += m_MoveDir;
		}
	}
}
