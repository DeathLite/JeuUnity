﻿using UnityEngine;
using System.Collections;

public abstract class PickupObjet : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartInitializations();
	}

    virtual protected void StartInitializations() { }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) OnContactWithPlayer();
    }

    abstract protected void OnContactWithPlayer();
	
	// Update is called once per frame
	void Update () {
	
	}
}
