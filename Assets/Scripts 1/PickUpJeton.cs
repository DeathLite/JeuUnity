﻿using UnityEngine;
using System.Collections;

public class PickUpJeton : PickupObjet {

	// Use this for initialization
	protected override void OnContactWithPlayer()
    {
        //DataManager.Instance.IncreaseScore();
        Destroy(gameObject);
	}

    static public int nbJetons = 0;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            nbJetons=nbJetons+100;
            Destroy(gameObject);
        }
    }
}
