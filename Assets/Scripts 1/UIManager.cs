﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UIManager : MonoBehaviour
{
    GameObject[] pauseObjects;
    private bool isPaused = false;

	// Use this for initialization
	void Start ()
    {
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        hidePaused();
	}

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
			isPaused = !isPaused;
        }
        if (isPaused)
        {
            showPaused();
        }
        else
        {
            hidePaused();
        }

    }

    public void Recommencer()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void Continuer()
    {
		if (isPaused) 
		{
			isPaused = !isPaused;
			hidePaused ();
		} 
		else 
		{
			hidePaused ();
		}
    }

    public void showPaused()
    {
        foreach(GameObject g in pauseObjects)
        {
			GameObject FPC = GameObject.FindWithTag ("Player");

            Time.timeScale = 0f;
            g.SetActive(true);
        }

		/*foreach (Camera c in  GameObject.FindObjectsOfType(typeof(Camera))) 
		{

			if ((c.name == "FirstPersonCharacter") && (c.transform.position.x > 30)) 
			{
				c.enabled = false;
			}
		}*/
    }

    public void hidePaused()
    {
        foreach (GameObject g in pauseObjects)
        {
			GameObject.Find("FPSController").SetActive(true);
            Time.timeScale = 1f;
            g.SetActive(false);
        }
    }

    public void LoadLevel(string level)
    {
        Application.LoadLevel(level);
    }

    public void Quitter()
    {
        Application.Quit();
    }

}
