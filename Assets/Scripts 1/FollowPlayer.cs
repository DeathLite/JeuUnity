﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : PickupObjet {

	Transform target;
	float mooveSpeed = 20f;
	float rotationSpeed = 5f;
	public Transform myTransform;
    static public int nbLifemob=2;
    static public int nbLife = 100;

	[SerializeField]
	public GameObject mobV3;

    protected override void OnContactWithPlayer()
	{
			Destroy (gameObject);
	}


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            print("PLAYER touché");
			nbLife=nbLife-2;
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Bullet"))
        {
            nbLifemob--;
            if (nbLifemob == 0)
            {
                print("detruire " + gameObject.name);
                Destroy(gameObject);
				PickUpJeton.nbJetons = PickUpJeton.nbJetons + 5;
				nbLifemob = 2;

            }
        }
    }


	void Start(){
		target = GameObject.FindWithTag ("Player").transform;
	}

	void Update(){
		transform.LookAt (new Vector3 (target.position.x, transform.position.y, target.position.z));
		myTransform.Translate (Vector3.forward * mooveSpeed * Time.deltaTime);
	}
}
