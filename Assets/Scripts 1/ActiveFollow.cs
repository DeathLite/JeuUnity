﻿using UnityEngine;
using System.Collections;

public class ActiveFollow : MonoBehaviour {

	Transform target;
	float mooveSpeed = 20f;
	float rotationSpeed = 5f;
	public Transform myTransform;
	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			target = GameObject.FindWithTag ("Player").transform;
	
		}
	}
	// Update is called once per frame
	void Update () {
		transform.LookAt (new Vector3 (target.position.x, transform.position.y, target.position.z));
		myTransform.Translate (Vector3.forward * mooveSpeed * Time.deltaTime);
	}
}
