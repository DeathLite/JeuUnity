﻿using UnityEngine;
using System.Collections;

public class SpawnObject : MonoBehaviour
{

    [System.Serializable]

    public class SpawnObjects
    {
        public string name;
        public GameObject obj;
        public int probability;
    }

    public Transform[] objectSpawns;
    public SpawnObjects[] objects_and_prob;
    private int prob_sum;

    // Use this for initialization
    void Start()
    {
        prob_sum = 0;
        foreach (SpawnObjects _obj in objects_and_prob)
            prob_sum += _obj.probability;
        Spawn();
    }

    void Spawn()
    {
        GameObject o;
        int num = 0;
        foreach (Transform objectSpawn in objectSpawns)
        {
        
            int random_value = Random.Range(0, prob_sum);
            int sum = 0; for (int i = 0; i < objects_and_prob.Length; i++)
            {
                sum += objects_and_prob[i].probability;
                if (random_value < sum)
                {
                    if (objects_and_prob[i].obj != null)
                    {
                        o = Instantiate(objects_and_prob[i].obj, objectSpawn.position, Quaternion.identity) as GameObject;
                        o.name = "MOB"+ num++;
                    }
                    break;
                }
            }

        }
    }
}