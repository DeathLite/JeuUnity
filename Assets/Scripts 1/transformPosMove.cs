﻿using UnityEngine;
using System.Collections;

public class transformPosMove : MonoBehaviour
{

#pragma strict

    Vector3 depl;
    float timeLimit;
	[SerializeField]
	float limiteur=5;
    // Use this for initialization
    void Start()
    {
        depl = new Vector3(0.0f, 0.0f, -0.25f);
        timeLimit = 0;
    }
    /*
        float timeLimit = 10.0f;
        float timeLimit2 = 1.0f;// 10 seconds.
        int a = 0;*/

    void Update()
    {

		if (timeLimit < limiteur) // Si timeLimit supérieur à 1, alors sa monte et on décrément timeLimit dans la condition.
        {
            transform.position = transform.position + depl;
            timeLimit += Time.deltaTime;
        }

        else
        {
            timeLimit = 0;
            depl *= -1;
        }
    }
}
