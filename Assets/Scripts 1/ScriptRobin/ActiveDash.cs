﻿using UnityEngine;
using System.Collections;

public class ActiveDash : PickupObjet {

	// Use this for initialization
	protected override void OnContactWithPlayer()
	{
		//DataManager.Instance.IncreaseScore();
		Destroy(gameObject);
	}
		

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			Dash.statement2 = true;
			Destroy(gameObject);
		}
	}
}