﻿using UnityEngine;
using System.Collections;

public class SpawnPlat : SpawnObject {

	static public bool statement=false;
	[SerializeField]
	public GameObject plat;
	public int compteurPlat=0;
	
	// Update is called once per frame
	void Update () {
		if (statement == true) 
		{
			if (Input.GetButtonDown("Fire2"))
				{		
						Spawn ();
						compteurPlat++;
				}
		}
		if (compteurPlat == 5) {
			statement = false;
			compteurPlat = 0;
		}
	}

	void Spawn()
	{
		// Create the Bullet 
		GameObject plateforme =  (GameObject) ( Instantiate (plat, transform.position, transform.rotation));

	
		// Destroy the bullet after 2 seconds
		Destroy(plateforme, 5.0f);
	}
}
