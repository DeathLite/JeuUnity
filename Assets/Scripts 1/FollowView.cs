﻿using UnityEngine;
using System.Collections;

public class FollowView : MonoBehaviour {

	Transform target;
	// Use this for initialization
	void Start () {
		target = GameObject.FindWithTag ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (new Vector3 (target.position.x, transform.position.y, target.position.z));
	}
}
