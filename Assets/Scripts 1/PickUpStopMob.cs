﻿using UnityEngine;
using System.Collections;

public class PickUpStopMob : PickupObjet {

	// Use this for initialization
	protected override void OnContactWithPlayer()
	{
		//DataManager.Instance.IncreaseScore();
		Destroy(gameObject);
	}

	static public bool nbMob = true;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			nbMob = false;
			Destroy(gameObject);
		}
	}
}
