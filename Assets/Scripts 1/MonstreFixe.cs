﻿using UnityEngine;
using System.Collections;

public class MonstreFixe : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			print("PLAYER touché");
			FollowPlayer.nbLife--;
			Destroy(gameObject);
		}

		if (other.gameObject.CompareTag("Bullet"))
		{
			FollowPlayer.nbLifemob--;
			if (FollowPlayer.nbLifemob == 0)
			{
				print("detruire " + gameObject.name);
				Destroy(gameObject);
				PickUpJeton.nbJetons = PickUpJeton.nbJetons + 5;
				FollowPlayer.nbLifemob = 2;

			}
		}
	}
}
