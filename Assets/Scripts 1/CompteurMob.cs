﻿using UnityEngine;
using System.Collections;

public class CompteurMob : MonoBehaviour {

	static public int compteurMob=0;
	// Use this for initialization
	void Start () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Bullet"))
		{
			compteurMob++;
		}
	}
	// Update is called once per frame
	void Update () {
		if (compteurMob > 5) {
			SpawnPlat.statement = true;
		}
	}
}
