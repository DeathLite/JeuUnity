﻿using UnityEngine;
using System.Collections;

public class PickUpEclair :  PickupObjet {

	// Use this for initialization
	protected override void OnContactWithPlayer()
	{
		//DataManager.Instance.IncreaseScore();
		Destroy(gameObject);
	}

	static public int nbEclair = 0;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			nbEclair++;
			Destroy(gameObject);
		}
	}
}