﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class ShootMonster : MonoBehaviour 
{
	public GameObject bullet;                // The enemy prefab to be spawned.
	public float spawnTime = 2f;            // How long between each spawn.
	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.



void Start ()
{
	// Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
	InvokeRepeating ("Spawn", spawnTime, spawnTime);
}


void Spawn ()
{

	// Find a random index between zero and one less than the number of spawn points.
	int spawnPointIndex = Random.Range (0, spawnPoints.Length);

	// Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
		GameObject balle =  (GameObject) ( Instantiate (bullet, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation));
		balle.GetComponent<Rigidbody>().velocity = balle.transform.forward * 250;
		Destroy(balle, 2.0f);
}


}


/*{

[SerializeField]
public GameObject bullet;                // The enemy prefab to be spawned.
public float spawnTime = 3f;            // How long between each spawn.
public Transform bulletSpawn;         // An array of the spawn points this enemy can spawn from.



void Start ()
{
	// Call the Spawn function after a delay of the spawnTime and then continue to call after the same amount of time.
	InvokeRepeating ("Spawn", spawnTime, spawnTime);
}


void Spawn ()
{

	// Find a random index between zero and one less than the number of spawn points.

	// Create an instance of the enemy prefab at the randomly selected spawn point's position and rotation.
		Instantiate (bullet, bulletSpawn.position, bulletSpawn.rotation);
}
		
}


/*	[SerializeField]
		public GameObject bullet;
		public Transform bulletSpawn;
		int a=0;

		// Update is called once per frame
		void Update()
		{
			StartCoroutine(Mycompteur());
		}

	IEnumerator Mycompteur()
	{
		while (a == 0) {
			yield return new WaitForSecondsRealtime (5.0f);
			Fire ();
	}
	
	}
		void Fire()
		{
		//StartCoroutine (compteur ());
			// Create the Bullet 
		GameObject balle =  (GameObject) ( Instantiate (bullet, transform.position, transform.rotation));

		// Add velocity 
		balle.GetComponent<Rigidbody>().velocity = balle.transform.forward * 250;

		// Destroy the bullet after 2 seconds
		Destroy(balle, 1.0f);
		}
	}

*/
