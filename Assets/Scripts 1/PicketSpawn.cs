﻿using UnityEngine;
using System.Collections;

public class PicketSpawn : PickupObjet {

	// Use this for initialization
	protected override void OnContactWithPlayer()
	{
		//DataManager.Instance.IncreaseScore();
		Destroy(gameObject);
	}
		

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			SpawnPlat.statement = true;
			Destroy(gameObject);
		}
	}
}