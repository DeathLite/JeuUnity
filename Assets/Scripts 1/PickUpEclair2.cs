﻿using UnityEngine;
using System.Collections;

public class PickUpEclair2 : PickupObjet {

	// Use this for initialization
	protected override void OnContactWithPlayer()
	{
		//DataManager.Instance.IncreaseScore();
		Destroy(gameObject);
	}

	static public int nbEclair2 = 0;

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag("Player"))
		{
			nbEclair2++;
			Destroy(gameObject);
		}
	}
}