﻿using UnityEngine;
using System.Collections;

public class transformPositionDown : MonoBehaviour {
	Vector3 depl;
	float timeLimit;


	// Use this for initialization
	void Start () {
		depl = new Vector3(0.0f, -0.1f, 0.0f);
		timeLimit = 0;

	}
	/*
    float timeLimit = 10.0f;
    float timeLimit2 = 1.0f;// 10 seconds.
    int a = 0;*/

	void Update () {

		if (timeLimit < 2) // Si timeLimit supérieur à 1, alors sa monte et on décrément timeLimit dans la condition.
		{
			transform.position = transform.position +depl;
			timeLimit += Time.deltaTime;
		}

		else  {
			timeLimit = 0;
			depl *= -1;
		}

	}
}
